## Banking App

#### Unresolved Issues

As at 19/5/2019

1. Can withdraw more than the balance. Error message and control structure (`isOverdrawn === false`) not kicking in
2. Unable to display a list of `history` array items under "Transaction History" after every successful, withdraw and deposit function runs. See `view.alwaysListening()`
3. The program is dependent upon calling on an initiated `account1` object. Can it be modified so that all functions and event listeners apply to any account object created
4. How to implement the MVC pattern in javascript?
5. what is

```
const name => name(function{
  console.log(person_name)
})
```

#### Braindump

- `.value`, `.valueAsNumber`, `parseInt()` all do different things

```

```
