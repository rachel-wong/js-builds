// ***** MODEL ****** //
// ACCOUNT OBJECT CONSTRUCTOR
function Account(name, balance) {
	this.name = name
	this.balance = balance
	this.history = [] // transaction history
	// PATTERN A TO DECLARE NATIVE METHODS FOR OBJECTS INITIALISED FROM A CONSTRUCTOR (INSIDE)
	//this.methodName = function(argument){}
}

// PATTERN B TO DECLARE NATIVE METHODS FOR OBJECTS INITIALISED FROM A CONSTRUCTOR (OUTSIDE)
// Object.prototype.methodName = function(argument) {}

// DEPOSIT
Account.prototype.deposit = function(amount) {
	if (this.isPositive(amount) == true) {
		this.balance += amount
		this.displayBalance(this.balance)
	}
}

// WITHDRAWAL
Account.prototype.withdraw = function(amount) {
	// If the amount is positive and is not greater than the current balance (isOverdrawn = false)
	if (this.isPositive(amount) === true && this.isOverdrawn(amount) === false) {
		console.log(`DEBUG: At Withdraw method, the amount is ${(typeof amount, amount)}`)
		console.log(`DEBUG: At Withdraw method, isOverdrawn is ${this.isOverdrawn(amount)}`)
		this.balance -= amount
		this.displayBalance(this.balance)
	}
}

// DISPLAY BALANCE
Account.prototype.displayBalance = function(balance) {
	console.log(`${this.name}'s account balance is currently ${balance}.`)
}

// CHECK: IF AMOUNT IS POSITIVE
Account.prototype.isPositive = function(amount) {
	if (amount > 0) {
		return true
	} else {
		console.error("Input a POSITIVE amount!")
		return false
	}
}

// CHECK: IF THERE IS ENOUGH BALANCE
Account.prototype.isOverdrawn = function(amount) {
	if (this.balance < amount) {
		console.error("Insufficient Funds!")
		return true
	} else {
		console.error("Go ahead!")
		return false
	}
}

// TESTING - CREATE ACCOUNT
const account1 = new Account("Bob", 100)
account1.displayBalance(account1.balance)
x	
// EVENT LISTENING AND CONTROLLER TOGETHER
// Grabbing everything from the HTML UI
var balanceDisplayTag = document.getElementById("balanceDisplayTag")
var displayBalanceButton = document.getElementById("displayBalanceButton")
var depositInput = document.getElementById("depositInput")
var depositButton = document.getElementById("depositButton")
var depositTag = document.getElementById("depositTag")
var withdrawInput = document.getElementById("withdrawInput")
var withdrawButton = document.getElementById("withdrawButton")
var withdrawTag = document.getElementById("withdrawTag")

// EVENT: Click on DISPLAY button
displayBalanceButton.addEventListener("click", function() {
	balanceDisplayTag.innerText = account1.balance
	account1.history.push(`Account balance logged as ${account1.balance}`)
})

// EVENT: Input and Click on DEPOSIT button
depositButton.addEventListener("click", function() {
	// Pass input amount to deposit method
	account1.deposit(depositInput.valueAsNumber)
	// Display the deposit action
	depositTag.innerText =
		"Deposited " + depositInput.valueAsNumber + " to " + account1.name + "'s account"
	// Add deposit action to history array
	account1.history.push(depositTag.innerText)
	console.log(`History array is added with ${account1.history}`)

	// Display the updated balance
	balanceDisplayTag.innerText = account1.balance
	depositInput.value = "" // clear the input
})

// EVENT: Input and Click on WITHDRAW button
withdrawButton.addEventListener("click", function() {
	// Pass input amount to withdraw method
	account1.withdraw(withdrawInput.valueAsNumber)
	// Display the withdraw action
	withdrawTag.innerText =
		"Withdrew " + withdrawInput.valueAsNumber + " from " + account1.name + "'s account"
	// Add withdraw action to history array
	account1.history.push(withdrawTag.innerText)
	console.log(`History array is added with ${account1.history}`)

	// Display the updated balance
	balanceDisplayTag.innerText = account1.balance
	withdrawInput.value = "" // clear the input
})

// ****** CONTROLLER ****** //
// HANDLERS for running different methods when onClick="" is triggered
var handlers = {}

// ****** VIEW ****** //
// EVENT LISTENERS (No logic)
var view = {
	// create an li of history whenever a display balance, withdraw, deposit method is completed successfully
	alwaysListening: function() {
		var historyList = document.querySelector("ul") // the transaction history list

		// for every addition to the account history array, create an li element with the update (withdrawTag, balanceDisplayTag, depositTag)
		account1.history.forEach(function(entry) {
			console.log("firing")
			var historyEntry = document.createElement("li")
			historyEntry.textContent = entry
			historyList.appendChild(historyEntry)
		})
	}
}

view.alwaysListening()
