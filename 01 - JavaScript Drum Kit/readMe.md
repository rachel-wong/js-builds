## Javascript30 - Drum Kit

#### Brain dump

When eventlistener calls a function, the function needs to accept the event as an argument

```
function addEffect(event){
  element.classList.add('.newEffect')
  console.log(event.property)
}
element.addEventListener('click', addEffect)
```

When an eventlistener fires a function, `this` refers to the element that the eventlistener is attached to

```
function addEffect(event){
  element.classList.add('.newEffect')
  console.log(this) // it is the element
}
element.addEventListner('click', addEffect)
```
