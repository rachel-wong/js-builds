## Budgee Budgety - an unoriginal calculator :moneybag: :money_with_wings:

An exercise opportunity to learn how to approach a problem scenario, block it out before implementation -- to answer not just the *what* but the *why's* in between the lines.

- data encapsulation hides implementation details from being overridden

### Basic IFFE 
```
let budgetController = (function () {
  // private variable
  let x = 23;
  // private add function
  let add = function (a) {
    return x + a;
  }
  return {
    // publicTest is publicly accessible object
    publicTest: function (b) {
      return add(b);
    }
  }
})(); // the ending() invokes 
```

```
// this needs to know about budget and UI to literally control it
let controller = (function (budgetCtrl, UICtrl) {
  let z = budgetCtrl.publicTest(5);
  return {
    anotherPublic: function () {
      return z // this needs to be a return statement, not a console.log()
    }
  }
})(budgetController, UIController) // invokes the controller by passing in the objects returned from budgetController and UIController
```

- event listening is set up in the `controller` class to pick up what is happening and delegate tasks to other controllers
