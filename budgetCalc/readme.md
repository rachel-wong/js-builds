## There aren't enough Budget Calculators in the world

#### Getting content to display with JS
1. `.innerHTML` parses content as HTML, so it takes longer.
- needs backticks
2. `.nodeValue` uses straight text, does not parse HTML, and is faster.
3. `.textContent` uses straight text, does not parse HTML, and is faster.
4. `.innerText` Takes styles into consideration. It won't get hidden text for instance.

#### Issue with making things disappear when using `setTimeout`
When using `this` inside the `setTimeout` or `setInterval` function, the callback needs to be a :star: **fatarrow** :star: and not a standard function declaration otherwise `this` will not be referring to the UI class but the Window object. 

In other words, this **works**
```
    setTimeout(() => {
      this.budgetFeedback.classList.remove('showItem')
    }, 4000)
```

This **WON"T** work
```
    setTimeout(function() {
      this.budgetFeedback.classList.remove('showItem')
    }, 4000)
```

To make that second version work, you need to reassign `this` to another variable and call that variable inside the `setTimeout` callback function, like so:
```
const self = this
    setTimeout(function() {
      self.budgetFeedback.classList.remove('showItem')
    }, 4000)
```

*So what's the takeaway?*

When in `setTimeout` and `setInterval` method, `this` changes to something else (a Window object) and will not be referring to the parent class or wherever you originally thought `this` was referring to. 

Always `console.log` the `this` to make sure you're referring to the right thing and when in doubt, reassign `this` to a variable just to be sure you're dealing with the right `this`. 

[Relevant Stackoverflow](https://stackoverflow.com/questions/7890685/referencing-this-inside-setinterval-settimeout-within-object-prototype-methods)


### Random Observations :eyes:
- any input values are `string` by default and you need to `parseInt()` them
- weirdly calculating the budget from string input results in a `number` budget, not `string`
- `showBalance()` could originally pass along values but we need it to be a standalone function call that is not reliant upon arguments so that the balance can be refreshed anytime there is a change to the expense or budget
- `showBalance()` is a standalone method that when it is called, it will draw the latest total expense and budget to recalcualte the budget to display on DOM

### How this thing works

> Sneaky footnote: this calculator only takes whole number values (no floating decimal values because pain :sob: :anger: :collision: .)

`DOMContentLoaded` fires when the HTML page is fully loaded onto browser. When this happens, it runs the `eventListeners()` standalone method. 

`eventListeners()` is a standalone function which creates separate eventListeners that fires for certain things happening on the HTML page
- the SUBMIT button being pressed on the budget form (accesses the UI object's submitBudgetForm method)
- the SUBMIT button being pressed on the expense form (accesses the UI object's submitExpenseForm method)
- anything happening on the expense list (accesses the UI object's )

When `DOMContentLoaded` event occurs (when the HTML page is fully loaded onto the browser), the `eventListeners()` is running in the background. `eventListeners()` will create a full instance of the `UI` class which will enable it to have access to its methods, variables. 

Concurrently, what is sitting in the background is a whole `UI` class which does the following
- create JS variable for every single DOM element on the HTMl page so that it can control them (add things, remove things, colour things, etc)
- set out individual methods for 
1. showBalance - grabs whatever the user inputs (expenses and balance), calculates it  and displays it on the page
2. submitBudgetForm -   
3. calculate totalExpense
4. submitExpenseForm
5. addExpense to the expense list

**`submitBudgetForm()`** is a controller-view method that 

It takes from the DOM: 
- the user inputted value for balance
- cleanse user input the balance input

Internally it thinks about: 
- calls the `showBalance()`

It displays on the DOM: 
- display the user input back onto the page as a DOM element

**`showBalance()`** is a controller -- view method that

It takes from the DOM: 
- takes the user inputted value from `submitBudgetForm()` as the balance amount 

Internally it thinks about:
- calls `totalExpense()` to return the total amount of expense from the `itemList` array. The `itemList` array is populated by `submitExpenseForm()`
- temporarily every expense input as an `amount` variable
- calculates the balance

It displays on the DOM:
- display the balance

**`submitExpenseForm`** is a controller-view method that

It takes from the DOM: 
- user input for each expense
- cleanse user input for the expense form
- creates an expense object for each item so that you have an id to use to refer to (delete) it later on (for edit functionalities)

It doesn't display. But tells `addExpense` to do the displaying
- sends each user-input along as an expense object to `addExpense`

`addExpense` is a view method that ONLY displays each item in the `itemList` array as a line with a edit and delete button that refers to the object id. It stores nothing and thinks about nothing.  

`submitBudgetForm` --> `showBalance` --> (`submitExpenseForm`) ---> `totalExpense` --> `addExpense` (display only)

## Implementing the Edit and Delete button for each expense item

**The general plan of attack for both edit/delete button**
- get the eventListener to capture the expense item that is being clicked and pass along the thing that has the object.id to the delete and edit function. the object.id helps to target the right expenseitem in the `itemList` to delete or edit
- create delete and edit functions in the UI class

For the **delete** function (the order of events matters)
- from the `eventListener()`, isolate the item.id
- remove the item from the DOM
- remove the object from the `itemList` with the same id 
- recalculate the balance

For the **edit** function (the order of events matters)
- from the `eventListener()`, isolate the item.id
- remove the item from the DOM
- remove the object from the `itemList` with the same id 
- **populate the `expenseForm` input fields with the title and the amount from the object that was being deleted**
- recalculate the balance

Refer to the comments in the code for implementation details, but few general notes from the sequence 

- `event.target` in eventListeners will return the thing being clicked on
- you can't just remove the thing you want by referring to it in javascript. you have to pinpoint it using `.parentElement` it, and then go up on level and then `.removeChild(the thing you want removed)`.
- `console.log` until you find the right `.parentElement`
  

