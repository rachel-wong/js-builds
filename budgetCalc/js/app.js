// class declaration needs to be above the line where an instance is declared
class UI {
  constructor() {
    // grab all the elements on the DOM
    this.budgetFeedback = document.querySelector(".budget-feedback");
    this.expenseFeedback = document.querySelector(".expense-feedback");
    this.budgetForm = document.getElementById("budget-form");
    this.budgetInput = document.getElementById("budget-input");
    this.budgetAmount = document.getElementById("budget-amount");
    this.expenseAmount = document.getElementById("expense-amount");
    this.balance = document.getElementById("balance");
    this.balanceAmount = document.getElementById("balance-amount");
    this.expenseForm = document.getElementById("expense-form");
    this.expenseInput = document.getElementById("expense-input");
    this.amountInput = document.getElementById("amount-input");
    this.expenseList = document.getElementById("expense-list");
    this.itemList = [];
    this.itemID = 0;
  }

  // Submit budget method
  submitBudgetForm() {
    // get value from the input form from the constructor
    const value = this.budgetInput.value

    // errorhandling for input value and display the error messages
    if (value === '' || value < 0) {
      this.budgetFeedback.classList.add('showItem') // make the feedback item appear by css
      this.budgetFeedback.innerHTML = `<p>Value cannot be empty  </p>`

      // Reassigning *this* is not required because using fatarrow functions
      // const self = this // this is not needed anymore
      // set a time to make the feedback box to disappear after 4 seconds
      setTimeout(() => {
        // referring to *this* won't work inside setTimeout if the callback is not using the fatarrow 
        // the *this* inside setTimeout refers to the Window
        this.budgetFeedback.classList.remove('showItem')
        // self.budgetFeedback.classList.remove('showItem') // not needed
      }, 4000)
    }
    // otherwise if the value is agreeable, display the amount on DOM 
    else {
      this.budgetAmount.textContent = value // display whatever you got from the INPUT onto the DOM again
      this.budgetInput.value = '' // reset the input form
      this.showBalance() // call showBalance() to update the balance amount
    }
  }

  // calculate and display balance on DOM
  showBalance() {
    const expense = this.totalExpense() // grab all the expenses

    // calculate the balance (here balance parseInt into a number. Otherwise input values are treated as strings)
    const balance = parseInt(this.budgetAmount.textContent) - expense
    // display balance on DOM
    this.balanceAmount.textContent = balance
    // show GREEN BALANCE TEXT for +ive balance
    // show RED BALANCE TEXT for -ive balance
    // show BLACK BALANCE TEXT for 0 balance 
    switch (true) {
      case balance < 0:
        this.balance.classList.remove('showGreen', 'showBlack')
        this.balance.classList.add('showRed')
        break
      case balance === 0:
        this.balance.classList.remove('showGreen', 'showRed')
        this.balance.classList.add('showBlack')
        break
      case balance > 0:
        this.balance.classList.remove('showRed', 'showBlack')
        this.balance.classList.add('showGreen')
        break
    }
  }

  // called by showBalance() and only calculates, no display
  // calculate and display all expenses added to
  totalExpense() {
    let total = 0

    // rather than passing along the expense value everytime the expense form's submit button is clicked, get the value directly from the constructor's itemList array 
    if (this.itemList.length > 0) {
      total = this.itemList.reduce((a, b) => {
        console.log(`total is ${a} and current amount is ${b.amount}`)
        a += b.amount
        return a
      }, 0) // 0 is the initial value
    }
    this.expenseAmount.textContent = total // if nothing in list, then show 0 on DOM
    return total // need to return total or it won't get passed to showBalance() to display on DOM
  }

  // grab individual expense and display on DOM and pass to totalExpense
  submitExpenseForm() {
    // grab the input from DOM on event
    const expenseName = this.expenseInput.value
    const expenseValue = this.amountInput.value

    // errorhandling for input value and display error messages (same as showBudget)
    if (expenseValue === '' || expenseName === '' || expenseValue < 0) {
      this.expenseFeedback.classList.add('showItem') // make the feedback item appear by css
      this.expenseFeedback.innerHTML = `<p>NEGATIVE or EMPTY values are not allowed.</p>`

      // set time for feedback box to disappear after 4 seconds
      setTimeout(() => {
        this.expenseFeedback.classList.remove('showItem')
      }, 4000)
    } else {

      let amount = parseInt(expenseValue) // initial input is a string so need to turn it into a NUMBER for calculation

      // Reset the input field
      // make sure you target the .value otherwise it will return undefined on subsequent submits after the first
      this.expenseInput.value = ''
      this.amountInput.value = ''

      // Create an expense object and pass it to the itemList array in the constructor
      let expenseItem = {
        id: this.itemID,
        title: expenseName,
        amount, // in es6, if propertyName is the same as the valueName, then shorten it
      }
      this.itemID++ // create a new itemID for the next item
      this.itemList.push(expenseItem)
      this.addExpense(expenseItem) // add list item on DOM
      this.showBalance() // need to trigger this or get NAN on DOM
    }
  }

  // for DOM display only (no calculating or storing of values)
  // create a single expense item to display on DOM as a list
  addExpense(expenseObj) {
    console.log("expenseObj", expenseObj)

    // create a parent div to appear on the DOM
    const div = document.createElement('div')
    div.classList.add('expense')

    div.innerHTML = `
    <div class="expense-item d-flex justify-content-between align-items-baseline">
         <h6 class="expense-title mb-0 text-uppercase list-item">${expenseObj.title}</h6>
         <h5 class="expense-amount mb-0 list-item">${expenseObj.amount}</h5>
         <div class="expense-icons list-item">
          <a href="#" class="edit-icon mx-2" data-id="${expenseObj.id}">
           <i class="fas fa-edit"></i>
          </a>
          <a href="#" class="delete-icon" data-id="${expenseObj.id}">
           <i class="fas fa-trash"></i>
          </a>
         </div>
        </div>
    `
    // stick the div as a child of expenseList item on DOM
    this.expenseList.appendChild(div)
  }

} // end of UI class

// triggered by entry point to always listen int he background
// trigger methods in the UI class whenever an event happens
function eventListeners() {

  // grab the forms so that you can attach event listeners to it 
  const budgetForm = document.getElementById('budget-form')
  const expenseForm = document.getElementById('expense-form')
  const expenseList = document.getElementById('expense-list')

  // Create an object instance of UI class with all the DOM elements & calc functions
  const ui = new UI()

  // listening for budget form submitting
  budgetForm.addEventListener("submit", event => {
    event.preventDefault()
    ui.submitBudgetForm()
  })

  // listening for expense form submitting
  expenseForm.addEventListener("submit", event => {
    event.preventDefault()
    ui.submitExpenseForm()
  })

  // listening for expense-list clicking
  expenseList.addEventListener("click", event => {

  })
}

// ENTRY POINT!!!
// the DOMContentLoaded is an event where the entire HTML page has been loaded
// when the entire page has been loaded, run the eventListeners method
document.addEventListener("DOMContentLoaded", function () {
  eventListeners()
})