// VERSION WITH EDIT LIST FUNCTIONALITIES

class UI {
  constructor() {
    this.budgetFeedback = document.querySelector(".budget-feedback");
    this.expenseFeedback = document.querySelector(".expense-feedback");
    this.budgetForm = document.getElementById("budget-form");
    this.budgetInput = document.getElementById("budget-input");
    this.budgetAmount = document.getElementById("budget-amount");
    this.expenseAmount = document.getElementById("expense-amount");
    this.balance = document.getElementById("balance");
    this.balanceAmount = document.getElementById("balance-amount");
    this.expenseForm = document.getElementById("expense-form");
    this.expenseInput = document.getElementById("expense-input");
    this.amountInput = document.getElementById("amount-input");
    this.expenseList = document.getElementById("expense-list");
    this.itemList = [];
    this.itemID = 0;
  }

  submitBudgetForm() {
    const value = this.budgetInput.value

    if (value === '' || value < 0) {
      this.budgetFeedback.classList.add('showItem')
      this.budgetFeedback.innerHTML = `<p>Value cannot be empty  </p>`

      setTimeout(() => {
        this.budgetFeedback.classList.remove('showItem')
      }, 4000)
    } else {
      this.budgetAmount.textContent = value
      this.budgetInput.value = ''
      this.showBalance()
    }
  }

  showBalance() {
    const expense = this.totalExpense()

    const balance = parseInt(this.budgetAmount.textContent) - expense
    this.balanceAmount.textContent = balance
    switch (true) {
      case balance < 0:
        this.balance.classList.remove('showGreen', 'showBlack')
        this.balance.classList.add('showRed')
        break
      case balance === 0:
        this.balance.classList.remove('showGreen', 'showRed')
        this.balance.classList.add('showBlack')
        break
      case balance > 0:
        this.balance.classList.remove('showRed', 'showBlack')
        this.balance.classList.add('showGreen')
        break
    }
  }

  totalExpense() {
    let total = 0

    if (this.itemList.length > 0) {
      total = this.itemList.reduce((a, b) => {
        console.log(`total is ${a} and current amount is ${b.amount}`)
        a += b.amount
        return a
      }, 0)
    }
    this.expenseAmount.textContent = total
    return total
  }

  submitExpenseForm() {
    const expenseName = this.expenseInput.value
    const expenseValue = this.amountInput.value

    if (expenseValue === '' || expenseName === '' || expenseValue < 0) {
      this.expenseFeedback.classList.add('showItem')
      this.expenseFeedback.innerHTML = `<p>NEGATIVE or EMPTY values are not allowed.</p>`

      setTimeout(() => {
        this.expenseFeedback.classList.remove('showItem')
      }, 4000)
    } else {

      let amount = parseInt(expenseValue)

      this.expenseInput.value = ''
      this.amountInput.value = ''

      let expenseItem = {
        id: this.itemID,
        title: expenseName,
        amount,
      }
      this.itemID++
      this.itemList.push(expenseItem)
      this.addExpense(expenseItem)
      this.showBalance()
    }
  }

  addExpense(expenseObj) {
    console.log("expenseObj", expenseObj)

    const div = document.createElement('div')
    div.classList.add('expense')

    div.innerHTML = `
    <div class="expense-item d-flex justify-content-between align-items-baseline">
         <h6 class="expense-title mb-0 text-uppercase list-item">${expenseObj.title}</h6>
         <h5 class="expense-amount mb-0 list-item">${expenseObj.amount}</h5>
         <div class="expense-icons list-item">
          <a href="#" class="edit-icon mx-2" data-id="${expenseObj.id}">
           <i class="fas fa-edit"></i>
          </a>
          <a href="#" class="delete-icon" data-id="${expenseObj.id}">
           <i class="fas fa-trash"></i>
          </a>
         </div>
        </div>
    `
    this.expenseList.appendChild(div)
  }

  // fires when the edit icon is pressed
  editExpense(element) {
    console.log('edit', element)
    let id = parseInt(element.dataset.id)
    console.log('id:', id)
    let parent = element.parentElement.parentElement.parentElement
    console.log('parent:', parent)

    // 1. remove the expense item from DOM
    this.expenseList.removeChild(parent) // need to go up one more level (from parent) to delete something from the DOM

    let expense = this.itemList.filter(item => {
      return item.id === id
    }) // expense is an ARRAY with the expense item OBJECT that matches the (currently clicked) id 
    console.log('expense:', expense)

    // 2. get the values of this current expense item to display on the input form 
    this.expenseInput.value = expense[0].title
    this.amountInput.value = expense[0].amount

    // 3. remove the expense item from the internal itemList array
    let tempList = this.itemList.filter(item => {
      return item.id != id
    }) // return a new temporary array without the matching (currently clicked) expense item 

    this.itemList = tempList // reassign the new collection of expenses back to itemList 

    // 3. recalculate the balance since the itemList of expense objects (and by extension totalExpense) has changed
    this.showBalance()

  }

  // fires when the delete icon is pressed
  deleteExpense(element) {
    let id = parseInt(element.dataset.id) // id of the expenseItem to be deleted
    let parent = element.parentElement.parentElement.parentElement // finding div with the className 'expense' for deleting

    // 1. remove the expense item from the DOM
    // expenseList is the grandparent to <div class ="expense"></div>
    this.expenseList.removeChild(parent) // need to go up one more level (from parent) to delete something from the DOM

    let expense = this.itemList.filter(item => {
      return item.id === id
    }) // expense is an array with the expense item that matches the (currently clicked) id 
    console.log('expense:', expense)

    // 2. remove the expense item from the internal itemList array
    let tempList = this.itemList.filter(item => {
      return item.id != id
    }) // return a new temporary array without the matching (currently clicked) expense item 

    this.itemList = tempList // reassign the new collection of expenses back to itemList 

    // 3. recalculate the balance since the itemList of expense objects (and by extension totalExpense) has changed
    this.showBalance()
  }

}

function eventListeners() {

  const budgetForm = document.getElementById('budget-form')
  const expenseForm = document.getElementById('expense-form')
  const expenseList = document.getElementById('expense-list')

  const ui = new UI()

  budgetForm.addEventListener("submit", event => {
    event.preventDefault()
    ui.submitBudgetForm()
  })

  expenseForm.addEventListener("submit", event => {
    event.preventDefault()
    ui.submitExpenseForm()
  })

  // Edit the list functionalities
  expenseList.addEventListener("click", () => {
    // console.log("expense list is clicked", event.target) // this prints out the <h6>

    // event.target revealed that it selected on the <i> when we select the trash/edit icons
    // we need to draw out the parent element of the <i> to get to the object id ${expense.id}
    // if the the thing clicked has a parent element that has a classList that indicates edit-icon or delete-icon
    if (event.target.parentElement.classList.contains('edit-icon')) {
      ui.editExpense(event.target.parentElement) // sends along the <a href=""></a> tag with the data-id attribute
    } else if (event.target.parentElement.classList.contains('delete-icon')) {
      ui.deleteExpense(event.target.parentElement) // sends along the <a href=""></a> tag with the data-id attribute
    }
  })
}

document.addEventListener("DOMContentLoaded", function () {
  eventListeners()
})