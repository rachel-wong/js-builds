##WesBos 05 - Flex Panels

#### Braindump

- flex:1; so that each panel evenly distribute the extra space in between them
- document.querySelectorAll() uses the full css class reference (includes the dot notation)
- document.querySelectorAll() can refer to just tags (no notation but still quotation marks)
- in evenlisteners, the firing function is referenced, not called. calling the function is only for when page is loading. The reference to the function will be taken care of by the addEVentlistners
- set up different css settings first. then listen for a `transitioned` event to trigger when elements have finished animating on the page
- when you create a variable/const to grab all the elements which returns a nodelist, use a forEach to dip into each one and run methods on it.

```
			const panel = document.querySelectorAll(".panel") // returns a nodelist consisting of five panel divs
			panel.forEach(panel => panel.addEventListener("click", toggleOpen)) // this listens for each panel div
```
