### What I learnt

- to sort something without mutating, call a separate function onto the inputs a and b
- `.replace(<the things to look for>, <what to replace with>)`
- use regex to tell `.replace()` what to look for
- `parent.appendChild(child)`
- `.innerHTML` fills the inside of the HTML tag
