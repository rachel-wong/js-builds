const bands = ['The Plot in You', 'The Devil Wears Prada', 'Pierce the Veil', 'Norma Jean', 'The Bled', 'Say Anything', 'The Midway State', 'We Came as Romans', 'Counterparts', 'Oh, Sleeper', 'A Skylit Drive', 'Anywhere But Here', 'An Old Dog'];

function strip(bandname) {
  // the double quotes replaces what is taken, which is empty and prevents an undefined
  // trim takes away any space at the very end of the string 
  return bandname.replace(/^(a |the |an )/i, "").trim()
}

const sorted_bands = bands.sort(function (a, b) {
  // for every string element in bands
  // split string into an array separated by spaces
  // sort array by for every array 
  // if the first element of the array is a The, An, Oh, A
  // move along to the next element
  // sort 
  // .join each array back into a string
  //return the bands sorted

  if (strip(a) > strip(b)) {
    return 1
  } else {
    return -1
  }
})

// display the full list of bands here
const list = document.querySelector('#bands')

sorted_bands.forEach(band => {
  let li = document.createElement('li')
  list.appendChild(li)
  li.innerHTML = band
  console.log(band)
})

//** DEMO VERSION **/
document.querySelector('#bands').innerHTML = sorted_bands.map(band => `<h6>${band}</h6>`).join("")