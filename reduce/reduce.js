// when you are selecting multiple elements by tag on DOM --> nodelist
// To make changes to a nodelist, you need to change it to an array
// To change nodelist to Array -> [...] or Array.from()

const timeNodes = Array.from(document.querySelectorAll("[data-time]")) // returns a nodelist object
// can't use setAttribute on a nodeList, only on one single element
timeNodes.forEach(node => {
	node.style.color = "green"
})
// console.log(timeNodes)

// .map changes from array of items to array of string
const seconds = timeNodes
	.map(node => node.dataset.time)
	.map(timeCode => {
		const [mins, secs] = timeCode.split(":").map(parseFloat)
		return mins * 60 + secs
	})
	.reduce((total, vidSeconds) => total + vidSeconds)

console.log(seconds)
let secondsLeft = seconds
const hours = Math.floor(secondsLeft / 3600) // get the clean number of hours
secondsLeft = secondsLeft % 3600 // get the seconds left from the hour  division

const mins = Math.floor(secondsLeft / 60) // convert remainder of hours into minutes
secondsLeft = secondsLeft % 60 // get clean number of seconds left from, the minutes division

// console.log(`Hours: ${hours}, Minutes: ${mins}, Seconds Remaining: ${secondsLeft}`)