### Braindump

```
const seconds = timeNodes
	.map(node => node.dataset.time) // break up every element in the nodelist
	.map(timeCode => {
		// break every node in the list by ":" into an array of number pairs
		const [mins, secs] = timeCode.split(":").map(parseFloat)
		return mins * 60 + secs // this will enable you to assign the result of the split to the constant seconds
		// console.log(mins, secs) // can't log this outside of the .map{}
		// at this stage returns an array of strings
	})
	.reduce((total, vidSeconds) => total + vidSeconds) // this sums all the individual arrays together into one single number of seconds
```

## Changing the style of elements on the page

#### To change one element tag

You can use setAttribute. If there are any other competing styles, this will need to go further down the .js file to work.

> var variableName = setAttribute.('style', 'css style goes here")

```
const firstP = document.querySelector("p")
firstP.setAttribute("style", "color:blue !important;")
```

#### To change multiple elements of a single tag type

When you select a tag uisng querySelectorAll that will return multiple elements, this will return a nodelist.

To use a nodelist, you need to change it into array using spread operator [...] or wrap the `document.querySelectorAll` with an `Array.from`.

You will need to loop through this and assign a style for each element in the array.

> node[i].style.<whateverattribute> = "value"

```
// example
const allLi = document.querySelectorAll("p")
for (i = 0; i < allLi.length; i++) {
	allLi[i].style.color = "red" // this changes the colour of all <p> text to red
}
```
