// Model stores and modify data
class Model {
  // creates a new Model object when this class is called
  constructor() {
    // starting data for early building
    // this.todos = [{
    //     id: 1,
    //     text: "Run a marathon",
    //     complete: false
    //   },
    //   {
    //     id: 2,
    //     text: "Plant a garden",
    //     complete: false
    //   }
    // ]

    // get any todos from the localstorage of the browser or return an empty array
    this.todos = JSON.parse(localStorage.getItem('todos')) || []
  }

  // PRIVATE METHOD
  // adds any todos to the localstorage and to refresh the display on the DOM
  _commit(todos) {
    this.onTodoListChanged(todos)
    localStorage.setItem('todos', JSON.stringify(todos))
  }

  // binds the displaying todolist handler in controller to the model 
  // this allows model to call/access the onTodoListChanged method natively from within the model class even though it originated from the controller class
  // basically grabs a controller method and make it native here in the model class
  bindTodoListChanged(callback) {
    this.onTodoListChanged = callback
  }

  // *** Model functions methods *** //
  // add a single todo item to the todos array
  addTodo(todoText) {
    // adds todo to the todos array and reassigned back to itself (essentially copying itself)
    // this.todos = [...this.todos, todo]
    // console.log("model: addTodo", this.todos)
    // this.onTodoListChanged(this.todos)

    // create a new todo in the todos array
    const todo = {
      id: this.todos.length > 0 ? this.todos[this.todos.length - 1].id + 1 : 1,
      text: todoText,
      complete: false,
    }
    this.todos.push(todo) // add it to the todos array

    this._commit(this.todos) // add the todos array to localstorage and display
  }

  editTodo(id, updatedText) {
    this.todos = this.todos.map(todo =>
      // if todo item id matches, change the text property to the input text
      todo.id === id ? {
        id: todo.id,
        text: updatedText,
        complete: todo.complete
      } : todo // otherwise leave the todos array and todo unchanged
    )
    // this.onTodoListChanged(this.todos)
    this._commit(this.todos)
  }

  // use .filter to return a new todos array that EXCLUDES the matching id
  deleteTodo(id) {
    this.todos = this.todos.filter(todo => todo.id !== id)
    // this.onTodoListChanged(this.todos)
    this._commit(this.todos)
  }

  // flip the complete boolean to its opposite
  toggleTodo(id) {
    this.todos = this.todos.map(todo =>

      // if the todo id matches, then flip complete boolean to its opposite
      todo.id === id ? {
        id: todo.id,
        text: todo.text,
        complete: !todo.complete
      } : todo // if no match, return the todo item as it is and make no change to the todos array
    )
    // this.onTodoListChanged(this.todos)
    this._commit(this.todos)
  }
}

class View {
  // creates a baseline set up of what you see in the browser (could do this in the html as well)
  constructor() {
    // app is the #root div
    this.app = this.getElement('#root')

    // creates a form for the todos
    this.form = this.createElement('form')

    // create a text input field with name, placeholder value
    this.input = this.createElement('input')
    this.input.type = 'text'
    this.input.placeholder = 'Think of stuff to do '
    this.input.name = 'todo'

    this.title = this.createElement('h1')
    this.title.textContent = 'Another Todo List but MVC'

    // creates a button with label
    this.submitButton = this.createElement('button')
    this.submitButton.textContent = 'Add'

    // todos will be parsed as ul
    this.todoList = this.createElement('ul', 'todo-list')

    // create a form with the input field and submitButton 
    this.form.append(this.input, this.submitButton)

    // add everything (title, form, todolist ul) onto the #root div that we called app
    this.app.append(this.title, this.form, this.todoList)

    this._temporaryTodoText // private variable to hold any change in the span of the todo item
    this._initLocalListeners() // call private method to get the changedText from the input field to be temporary variable 
  }

  // PRIVATE method - listens for change in the input field
  // any time there is a change, set the edited text as the temporary variable
  _initLocalListeners() {
    this.todoList.addEventListener('input', event => {
      if (event.target.className === 'editable') {
        this._temporaryTodoText = event.target.innerText
      }
    })
  }

  // *** View Helper methods *** //
  createElement(tag, className) {
    const element = document.createElement(tag)
    // if the class provided does exists, add it to the element 
    if (className) element.classList.add(className)
    return element
  }

  // get whatever it is on the page by the id and return it as a variable
  getElement(selector) {
    const element = document.querySelector(selector)
    return element
  }

  // PRIVATE methods (underscore) and not used outside of the view class
  // getter
  get _todoText() {
    return this.input.value
  }
  // setter
  _resetInput() {
    this.input.value = ""
  }

  displayTodos(todos) {
    // create ul, li to list each item of todos array
    // any delete, add, change -> call todos from model, reset and redisplay list

    //delete all nodes before check
    while (this.todoList.firstChild) {
      this.todoList.removeChild(this.todoList.firstChild)
    }

    //Dislay default message when there are no todos
    if (todos.length === 0) {
      const p = this.createElement('p')
      p.textContent = 'Nothing to do! Add a task?'
      this.todoList.append(p)
    } else {
      // from the array of todos
      todos.forEach(todo => {
        // CREATE LI for every todo in the array
        const li = this.createElement('li')
        li.id = todo.id // assign an id attribute to every li which is also the todo id

        // CHECKBOX for each todo in the array
        const checkbox = this.createElement('input')
        checkbox.type = 'checkbox'
        checkbox.checked = todo.complete // set the checked to the complete bool

        // SET TEXT for each todo to be content editable
        const span = this.createElement('span')
        span.contentEditable = true
        span.classList.add('editable')

        // SET TEXT APPEARANCE based on the complete boolean value
        // if IS COMPLETE
        if (todo.complete) {
          const strike = this.createElement('s') // the <s> html tag is a strikethrough p
          strike.textContent = todo.text
          span.append(strike) // replace the span that was created above 
        }
        // if NOT COMPLETE
        else {
          span.textContent = todo.text // keep the span text as it is without any strikethrough
        }

        // CREATE DELETE button for each todo in array
        const deleteButton = this.createElement('button', 'delete') // delete is classname
        deleteButton.textContent = 'Delete' // set label for the button

        // ADD CHECKBOX + TEXT + DELETE BUTTON to each todo item 
        li.append(checkbox, span, deleteButton)

        // APPEND each li created to ul
        this.todoList.append(li)
      })
    }
  }

  // EVENT LISTENERS 

  // *** ADD TODO - FOR THE FORM *** //
  // whenever the FORM submits, send along whatever that is given in the input value to the event handler method and AFTERWARDS reset the form to empty
  bindAddTodo(handler) {
    this.form.addEventListener('submit', event => {
      event.preventDefault() //stops the form from automatically submitting to the page and wait till we actually press enter or submit

      if (this._todoText) {
        handler(this._todoText) // getter
        this._resetInput() //setter
      }
    })
  }
  // **** DELETE TODO - FOR THE DELETE BUTTON ***//
  // on the ul where there is a delete button, pass along to the event handler the id of the todo 
  bindDeleteTodo(handler) {
    this.todoList.addEventListener('click', event => {
      if (event.target.className === 'delete') {
        const id = parseInt(event.target.parentElement.id)
        handler(id)
      }
    })
  }

  // **** TOGGLE COMPLETE - FOR THE CHECKBOX ***//
  // on the ul where there is a checkbox, pass along to the event handler the id of the todo 
  bindToggleTodo(handler) {
    this.todoList.addEventListener('change', event => {
      if (event.target.type === 'checkbox') {
        const id = parseInt(event.target.parentElement.id)
        handler(id)
      }
    })
  }

  // *** EDIT TODO - FOR INPUT FIELD ***//
  bindEditTodo(handler) {
    this.todoList.addEventLister('focusout', event => {
      if (this._temporaryTodoText) {
        const id = parseInt(event.target.parentElement.id) // get the right id for the todo
        handler(id, this._temporaryTodoText) // pass the 
        this._temporaryTodoText = "" // reset the temporary variable
      }
    })
  }
}

class Controller {
  // creates a new Controller object which takes in two other objects, view & model
  constructor(model, view) {
    this.model = model
    this.view = view

    // Display INITIAL todos
    // pass along whatever that is in the model to the function to tell view's displayTodo function to show on DOM
    this.onTodoListChanged(this.model.todos)

    // this sets up a call to the view event listener and pass along a controller event handler
    // event listening comes first to pass along the event handling second
    this.view.bindAddTodo(this.handleAddTodo)
    this.view.bindDeleteTodo(this.handleDeleteTodo)
    this.view.bindToggleTodo(this.handleToggleTodo)
    this.view.bindEditTodo(this.handleEditTodo)
    // sets up a call to the model and passing along the onTodoListChanged so that model can access it directly
    this.model.bindTodoListChanged(this.onTodoListChanged)
  }

  // call view's displayTodo everytime todo changes
  // "todos" argument here is the this.model.todos above
  // need to bind this in the model 
  onTodoListChanged = todos => {
    this.view.displayTodos(todos)
  }

  // EVENT HANDLERS trigger changes to the MODEL
  handleAddTodo = todoText => {
    this.model.addTodo(todoText)
  }

  handleEditTodo = (id, todoText) => {
    this.model.editTodo(id, todoText)
  }

  handleDeleteTodo = id => {
    this.model.deleteTodo(id)
  }

  handleToggleTodo = id => {
    this.model.toggleTodo(id)
  }
}

// create an instance of the whole app (controller) to attach to the DOM via the view class
const app = new Controller(new Model(), new View())