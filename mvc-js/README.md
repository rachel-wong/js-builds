# Demonstrating MVC in Javascript (Yet another todo list)

** javascript can be a huge pooh ** :poop: :poop: :poop: 

[Reference](https://www.taniarascia.com/javascript-mvc-todo-app/)

### Base structure
- Set up separate classes for model, view, controller
- create blank constructors in each class to allow you create objects of that class when called
- initialise a controller constant that takes in a model view because the controller *controls* the view and model objects
- MODEL: set up the CRUD methods
- VIEW: getter, setter, elements in DOM
- CONTROLLER: initialise instance of the app (passing in model and view), set up the event handling triggers to call things  

#### MODEL
- in the constructor, create base data
- within the *class*, creates the methods for adding, editing and deleting todos
- the data will eventually need to be stored in localStorage so that it persists even when you refresh the browser
- if you test this out in node, copy/paste the class into terminal. create the app from the model class. then call `app.addTodo({id: 3, text: 'I am here', completed: false})`

#### VIEW
- connects with the browser DOM tree (the html, css etc)
- textContent is similar to innerText properties. textContent grabs everything whereas innerText only returns visible elements. 
- not 100% sure why we need to clear the nodes before adding the lis to the doms. is it to refresh everytime? 
- doesn't talk to Model, controller does this 
- listens to submit, click and change events 

#### CONTROLLER
- links between the model and controller
- it triggers calling the data from the model and passing it along to a view method `displayTodos`
- controller does eventhandling
- create event handlers in the controller. but need to modify VIEW to make sure those handlers are triggereed when certain things happen in the DOM. So even listeners are required in the VIEW
- event handlers are all arrow functions to avoid using `.bind(this)`
- in constructor, call the view event listener methods and pass along the controller's own event handler methods so that view knows which event handler to call when something happens. this needs to be added in the constructor of the controller class
- at this point even if there are event listeners in the view and event handlers in the controller, the model still needs to tell the controller that something has happened and redisplay the todolist after the even thas happened. 

So from the model, set up a bindTodoListChanged method that takes a callback, which is essentially `onTodoListChanged` method from controller. 

And in the controller constructor, make a call to the model's `bindTodolistChanged` method and pass along its own onTodoListChanged. 

This way the method can be called/accessed natively from within the model class, even though it belonged in the controller class/ I think. :confounded: :confused: So in the model, after every method, call the `ontodoListChanged`. 

This function is then added to a private method `_commit` in model class and it is called at the end of every model function method. 

### Local Storage
- make the todos array in the model constructor to be grabbing whatever that is in the localstorage
- use `setItem` in the `_commit` method in model class to set whatever that is the `todos` arrray to the localStorage
- all in the model class

## Editing function
- the span's `contentEditable` attribute makes the textContent live-editable (see view class)
- not calling the editTodo function everytime a keystroke change is made in the span

Essentialy in three main steps: 
- in view constructor, use a temporary holding variable to store the change 
- in view constructor, call a private method `_initLocalListener` that grabs temporary holding variable from the span
- in view class, create the event listener `bindEditTodo` attached to the ul to pass the span value back to the model via event handler.
- in controller class, call the `bindEditTodo` in view by passing the `editTodo` method from the controller

## The way the handlers, event listeners and model updates work

(This is backwards when things do happen)

in MODEL, there is a `modelAction` method for every CRUD function. It updates the `todos` array and the localStorage. 

in CONTROLLER, there is `handleControllermethod` which updates the model by CRUD data by calling `this.model.modelAction`.

in CONTROLLER, call `this.view.binderMethod(this.handleControllermethod)`

in VIEW, `binderMethod(handler)` receives the `handleControllermethod`. The `binderMethod` itself listens for a kind of event (focusout, change, input), checks where the event originated from (i.e.`event.target.name` is `checkbox`), find its `parentElement.id` for the element where the event is happening, and passes the id back to the `handleControllerMethod` to know which todo to modify. 