// SETTING UP A TODO LIST THAT UPDATES THE PAGE
// Grab the elements from the page
const form = document.querySelector("form")
const button = document.querySelector("button")
const ul = document.querySelector("ul")
const input = document.querySelector("item")

let itemsArray = [] // create a list of items that can be added to localStorage and returned
// Always set the localStorage first by default
localStorage.setItem("items", JSON.stringify(itemsArray))
// If localStorage items exist, then return it parsed otherwise return an empty array
const data = localStorage.getItem("items") ? JSON.parse(localStorage.getItem("item")) : []

// CREATE A NEW LI for everytime text is inputted into the field
// const name = argument => {operations of the function}
// assign constant with a callback, the operation of which is assigned to a constant
const liMaker = inputText => {
	const li = document.createElement("li") // create an li element
	li.textContent = inputText // assign the text input to the li element
	ul.appendChild(li) // add the li element within the ul
}

// FORM LISTENER TO ADD THINGS TO THE PAGE
form.addEventListener("submit", e => {
	e.preventDefault() // Stops the form from sending submit by default when it generates on the page and instead wait for us to press enter to submit
	itemsArray.push(input.value) // pushes the input into the itemsArray which goes into localStorage
	localStorage.setItem("items", JSON.stringify(itemsArray))
	// JSON.stringify converts any normal javacsript object into JSON String
	liMaker(input.value) // Tells liMaker function to handle all the adding li to the page

	input.value = "" // reset the form so that it's blank again for the next input value
})

// create a list of objects from what is inside the data object
data.forEach(item => {
	liMaker(item) //for every item in localStorage, create an li
})

// listening on button which clears local storage and the first li on the list
button.addEventListener("click", function() {
	localStorage.clear()
	while (ul.firstChild) {
		ul.removeChild(ul.firstChild)
	}
})
