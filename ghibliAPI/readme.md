## Studio Ghibli API Experiment

Trying to work out what does 'playing around with API" actually means in practicality. Specifically:

- What a Web API is.
- use the HTTP request GET with JavaScript
- create/display HTML elements using JavaScript.

[Reference](https://www.taniarascia.com/how-to-connect-to-an-api-with-javascript/)

[Reference](https://github.com/taniarascia/sandbox/tree/master/ghibli)

#### Braindump

- API = Application Program Interface
- software to software
- Web API: servers to talk to 3rd party software. Web server using HTTP to get URL endpoint that has JSON data
- HTTP requests has CRUD (Post, get, put/patch, delete)
- HTTP Status codes: 200 is success, 404 is error (not found)
- once the response is parsed by `JSON.parse(this.response)`, it returns an ARRAY OF OBJECTS, which you can then access the individual properties of each like below

```
	var data = JSON.parse(this.response)
	console.log(typeof data)
	console.log(data) // returns an array
  console.log(data[0].title)
```

- set up a condition range to capture what needs to happen if the connection does happen successfully, and likewise set up error messages in case if it hits a 404 or 200

```
if (request.status >= 200 && request.status < 400){
	// display the data here
} else{
	console.error('nothing is working')
}
- use `.textContent` to populate variables of html elements like <p> and <h1>
```

- `variable.setAttribute = ("class or id", "name of the class or id")`

### Basic Structure/Approach of this project

```
//Create a bunch of html elements at the top that sets up the framework for how the JSON data will get displayed on the page.

const app = document.getElementById("whatever it is") ...

// Create a Response object here

var request = new XMLHttpRequest()

// open up a connection to the API using GET
request.open("method type GET, POST, PATCH, DELETE", "the url to the api", true)

// when the connection has been made, the JSON result can be loaded

request.onload = function(){

	// change the json into an array of objects
	var data = JSON.parse(this.response)

	// set up a control structure here so that you can control what gets displayed on the DOM depending if the connection is successful or not

	if (request.status >= 200 && request.status < 400){

		// if you are successful, then go ahead
	data.forEach(itemOfData =>{

		// this is where you create variables, attach attributes of the data object to display onto the page.

	})
	} else {
		// if you are not successful, this is where you handle error messages
	}

}

// last but not least, you have to send the request object that you created way above

request.send()

```
