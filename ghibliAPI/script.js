// 4. DISPLAYING THE THINGS ON THE PAGE

// grab the page element
const app = document.getElementById("root")

// create and then attach an image onto the browser page itself
const logo = document.createElement("img") // creates an empty img
logo.src = "logo.png" // attaches the file to the img tag
const container = document.createElement("div") // create an empty div
container.setAttribute("class", "container") // call the empty div a container
app.appendChild(logo) // attach the logo into the root div of the page
app.appendChild(container) // attach the container div into the root div of the page
// the img and the container is not visible in the HTML code, they are only added to the browser DOM

// 2. RETRIEVING THE DATA USING HTTP REQUEST

// creating a new XMLHttpRequest fetch request object
var request = new XMLHttpRequest()

// opens a connection to the API using GET on the URL endpoint
request.open("GET", "https://ghibliapi.herokuapp.com/films", true)

// when the request is finished, we access JSON data from here
request.onload = function () {
	// console.log(request)
	// console.log(this.request)

	// 3. DEALING WITH THE JSON RESPONSE
	// This needs to be inside the onload because when the request is loaded, then deal with the JSON Data (not outside! - obviously)

	var data = JSON.parse(this.response)
	// console.log(typeof data)
	// console.log(data) // returns an array
	// console.log(data[0].title)

	// 5. Generate a separate card for each movie that is returned from JSON
	// if the states are within normal range (not error codes, 200 is connection error and 400 is not found)
	if (request.status >= 200 && request.status < 400) {
		// print the names of each movie from the JSON data
		data.forEach(movie => {
			// console.log(movie.title) // returns string
			// console.log(movie.description) // returns string

			// Creates a card for each of the movie that is returned from the JSON data object
			const card = document.createElement('div') // create a card constant object 
			card.setAttribute('class', 'card') // set the card-div attribute with a class="card"

			const h1 = document.createElement('h1') // create a heading
			h1.textContent = movie.title

			const p = document.createElement('p') // create a paragraph tag
			// p.textContent = movie.description // this just adds the full description to the p tag
			movie.description = movie.description.substring(0, 300) // returns a string
			// console.log(typeof movie.description) // the text will get cut off because the boxes are not big enogh
			p.textContent = `${movie.description}...` // add the movie description to the p tag

			// first add a card to the container, the order of what happens first MATTERS
			container.appendChild(card)
			// add the h1 and p to each of the card divs
			card.appendChild(h1)
			card.appendChild(p)
		})
	} else {

		// 6. Set up error handling in case if data returned hits a 404 or 200 
		const errorMessage = document.createElement('marquee')
		errorMessage.textContent = 'Not working'
		app.appendChild(errorMessage)
		console.log("ERROR")
	}
}

// 1. send the XMLHTTPRequest once the page has loaded
request.send()