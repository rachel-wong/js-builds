// an array of objects
const companies = [
	{ name: "Company One", category: "Finance", start: 1981, end: 2003 },
	{ name: "Company Two", category: "Retail", start: 1992, end: 2008 },
	{ name: "Company Three", category: "Auto", start: 1999, end: 2007 },
	{ name: "Company Four", category: "Retail", start: 1989, end: 2010 },
	{ name: "Company Five", category: "Technology", start: 2009, end: 2014 },
	{ name: "Company Six", category: "Finance", start: 1987, end: 2010 },
	{ name: "Company Seven", category: "Auto", start: 1986, end: 1996 },
	{ name: "Company Eight", category: "Technology", start: 2011, end: 2016 },
	{ name: "Company Nine", category: "Retail", start: 1981, end: 1989 }
]

// array of numbers
const ages = [33, 12, 20, 16, 5, 54, 21, 44, 61, 13, 15, 45, 25, 64, 32]

// base for loop
// for (let i = 0; i < companies.length; i++) {
// 	console.log(companies[i]) // prints out the object
// 	console.log(companies[i].name, companies[i].category, companies[i].start, companies[i].end)
// }

// **** forEach ****

// synchronous callback
companies.forEach((iterator, index, entireArray) => {
	console.log(iterator)
	console.log(iterator.name)
})

// **** FILTER ****
// get ages 21 and over

let canDrink = []
for (let i = 0; i < ages.length; i++) {
	// if current iteration is greater than or equal than 21
	if (ages[i] >= 21) {
		canDrink.push(ages[i])
	}
}
console.log(canDrink) // return all 21 and over ages in a new array

const canDrink = ages.filter(age => {
	if (age >= 21) {
		return true
	}
})
console.log(canDrink)

// CONVENTION: array.filter(argument => condition with argument)
const icanDrink = ages.filter(age => age >= 21)

const retailers = companies.filter(company => company.category === "Retail")
console.log(retailers)
// **** MAP **** //
const companyNames = companies.map(function(company) {
	return company.name
})

const test = companies.map(company => {
	return `${company.name} [${company.start} - ${company.end}]`
})

const agesSquare = ages.map(age => age * 2)
console.log(agesSquare)

// **** sort **** //

// Sort companies by start year
const sortedCompanies = companies.sort(function(c1, c2) {
	if (c1.start > c2.start) {
		return 1
	} else {
		return -1
	}
})

const sortedCompanies2 = companies.sort((c1, c2) => (c1.start > c2.start ? 1 : -1))
console.log(sortedCompanies2)

// Sort ages

const sortedAges = ages.sort() // wont' work

const ascAges = ages.sort((a, b) => a - b)
const descAges = ages.sort((a, b) => b - a)

console.log(ascAges)
console.log(descAges)

// **** reduce **** //
let ageSum = 0
for (let i = 0; i < ages.length; i++) {
	ages += ages[i]
}

const ageSum2 = ages.reduce(function(total, age) {
	return total + age
}, 0)

// array.reduce((runningTotal, arrayItem) => {return operation}, startingposition)

const ageSum3 = ages.reduce((total, age) => {
	return total + age
}, 0)

const ageSum4 = ages.reduce((total, sum) => total + age, 0)
console.log(ageSum4)

const totalYears = companies.reduce(function(total, company) {
	return total + (company.end - company.start)
}, 0)

const totalYears = companies.reduce((total, company) => total + (company.end - company.start), 0)

console.log(totalYears)
// an array of objects
const companies = [
	{ name: "Company One", category: "Finance", start: 1981, end: 2003 },
	{ name: "Company Two", category: "Retail", start: 1992, end: 2008 },
	{ name: "Company Three", category: "Auto", start: 1999, end: 2007 },
	{ name: "Company Four", category: "Retail", start: 1989, end: 2010 },
	{ name: "Company Five", category: "Technology", start: 2009, end: 2014 },
	{ name: "Company Six", category: "Finance", start: 1987, end: 2010 },
	{ name: "Company Seven", category: "Auto", start: 1986, end: 1996 },
	{ name: "Company Eight", category: "Technology", start: 2011, end: 2016 },
	{ name: "Company Nine", category: "Retail", start: 1981, end: 1989 }
]

// array of numbers
const ages = [33, 12, 20, 16, 5, 54, 21, 44, 61, 13, 15, 45, 25, 64, 32]

// base for loop
for (let i = 0; i < companies.length; i++) {
	console.log(companies[i]) // prints out the object
	console.log(companies[i].name, companies[i].category, companies[i].start, companies[i].end)
}

// **** forEach ****

// synchronous callback
companies.forEach((iterator, index, entireArray) => {
	console.log(iterator)
	console.log(iterator.name)
})

// **** FILTER ****
// get ages 21 and over

let canDrink = []
for (let i = 0; i < ages.length; i++) {
	// if current iteration is greater than or equal than 21
	if (ages[i] >= 21) {
		canDrink.push(ages[i])
	}
}
console.log(canDrink) // return all 21 and over ages in a new array

const canDrink = ages.filter(age => {
	if (age >= 21) {
		return true
	}
})
console.log(canDrink)

// CONVENTION: array.filter(argument => condition with argument)
const icanDrink = ages.filter(age => age >= 21)

const retailers = companies.filter(company => company.category === "Retail")
console.log(retailers)
// **** MAP **** //
const companyNames = companies.map(function(company) {
	return company.name
})

const test = companies.map(company => {
	return `${company.name} [${company.start} - ${company.end}]`
})

const agesSquare = ages.map(age => age * 2)
console.log(agesSquare)

// // **** sort **** //

// Sort companies by start year
const sortedCompanies = companies.sort(function(c1, c2) {
	if (c1.start > c2.start) {
		return 1
	} else {
		return -1
	}
})

const sortedCompanies2 = companies.sort((c1, c2) => (c1.start > c2.start ? 1 : -1))
console.log(sortedCompanies2)

// Sort ages

const sortedAges = ages.sort() // wont' work

const ascAges = ages.sort((a, b) => a - b)
const descAges = ages.sort((a, b) => b - a)

console.log(ascAges)
console.log(descAges)

// **** reduce **** //
let ageSum = 0
for (let i = 0; i < ages.length; i++) {
	ages += ages[i]
}

const ageSum2 = ages.reduce(function(total, age) {
	return total + age
}, 0)

// array.reduce((runningTotal, arrayItem) => {return operation}, startingposition)

const ageSum3 = ages.reduce((total, age) => {
	return total + age
}, 0)

const ageSum4 = ages.reduce((total, sum) => total + age, 0)
console.log(ageSum4)

const totalYears = companies.reduce(function(total, company) {
	return total + (company.end - company.start)
}, 0)

const totalYears = companies.reduce((total, company) => total + (company.end - company.start), 0)

console.log(totalYears)

// Combine Methods

const combined = ages
	.map(age => age * 2) // times each age by 2
	.filter(age => age >= 40) // get rid of anything less than 40
	.sort((a, b) => a - b) // sort
	.reduce((total, b) => total + b, 0) // add them all together in the end
console.log(combined)
