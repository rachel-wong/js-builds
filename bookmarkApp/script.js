document.getElementById('myForm').addEventListener('submit', saveBookmark)
document.getElementById('clearBookmarks').addEventListener('click', clearAll)

function fetchBookmarks() {
  let results = document.getElementById('bookmarksResults')
  let div = document.createElement('div')
  results.appendChild(div)

  let bookmarks = JSON.parse(localStorage.getItem("bookmarks"))
  // for every bookmark, create a separate div inside the parent div
  if (bookmarks != null) {
    for (let mark of bookmarks) {
      let name = mark.name
      let url = mark.url
      div.innerHTML +=
        '<div class="well">' +
        '<h3>' + name +
        ' <a class="btn btn-default" target="_blank" href="' + addhttp(url) + '">Visit</a> ' +
        ' <a onclick="deleteBookmark(\'' + url + '\')" class="btn btn-danger" href="#">Delete</a> ' +
        '</h3>' +
        '</div>'
    }
  } else {
    results.innerHTML = "<h2 style='text-align:center'>No bookmarks</h2>" // setting it to results stops it from reloading multiple times
  }
}

// uses a url to match the item
function deleteBookmark(url) {
  // changing the localStorage to version without a matching url
  let bookmarks = JSON.parse(localStorage.getItem("bookmarks"))

  for (let i = 0; i < bookmarks.length; i++) {
    if (bookmarks[i].url == url) {
      bookmarks.splice(i, 1) // mutate existing array
    }
  }
  localStorage.setItem("bookmarks", JSON.stringify(bookmarks))
  fetchBookmarks() // redisplay
}


function saveBookmark(event) {
  event.preventDefault() // stops form from reloading automatically 
  // Grab all values
  let siteName = document.getElementById('siteName').value
  let siteUrl = document.getElementById('siteUrl').value

  if (!validateForm(siteName, siteUrl)) {
    return false // drops out 
  }

  let bookmark = {
    name: siteName,
    url: siteUrl,
  }

  let bookmarks = JSON.parse(localStorage.getItem("bookmarks")) // array 
  if (bookmarks === null) {
    let bookmarks = []
    bookmarks.push(bookmark)
    localStorage.setItem("bookmarks", JSON.stringify(bookmarks))
  } else {
    bookmarks.push(bookmark)
    localStorage.setItem("bookmarks", JSON.stringify(bookmarks))
  }
  //reset form
  document.getElementById('myForm').reset()
  fetchBookmarks() //redisplay
}

// Clears localStorage and all bookmarks
function clearAll() {
  localStorage.clear()
  // console.log('localStorage:', localStorage)
  fetchBookmarks()
  alert("You've deleted all your bookmarks")
}

// valid url and not empty
function validateForm(siteName, siteUrl) {
  var expression = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi // URL validator
  var regex = new RegExp(expression)
  if (!siteName || !siteURL) {
    alert("Please fill in the form.")
    return false // stop from keep submitting in the form
  }
  if (!siteUrl.match(regex)) {
    alert("Please input a real URL")
    return false
  }
  return true
}

// Check for any http:// headers in the user submitted data
function addhttp(url) {
  if (!/^(?:f|ht)tps?\:\/\//.test(url)) {
    url = "http://" + url
  }
  return url
}