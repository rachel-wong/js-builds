## Vanilla JS Bookmark Application

[Reference](https://www.youtube.com/watch?v=DIVfDZZeGxM)

Bookmarks are stored in localStorage

### Some Observations :eyes:

Again the entry point here is in the HTML `<body>` tag 
> <body onload="fetchBookmarks()">

Largely the logic remains exactly the same as the issues tracker. But inputting more form validation.