let formSubmit = document.getElementById('myForm').addEventListener('submit', saveBookmark)
let clearButton = document.getElementById('clearBookmarks').addEventListener('click', clearAll)

// runs on <body> load on DOM
function fetchBookmarks() {
  let bookmarks = JSON.parse(localStorage.getItem("bookmarks"))
  console.log('bookmarks:', bookmarks)
  let results = document.getElementById("bookmarksResults")

  if (bookmarks != null) {
    for (let mark of bookmarks) {
      let id = mark.id
      let name = mark.name
      let url = mark.url
      let date = mark.date
    }
    let div = createElement('div')
    div.classList.add("singleResult")

    div.innerHTML = `<h3>${name}</h3>
    <p>Saved on: ${date}</p>
    <a href="${url}">Go to Site</a>
    ` + "<a href='#' onclick='deleteBookmark(/''+id'/>')'>Delete Bookmark </a>"
    ">Hello</a>"
    results.appendChild(div)
  } else {
    alert("You need to add some bookmarks first!")
  }
}

function saveBookmark(event) {
  let name = document.getElementById('siteName').value
  let url = document.getElementById('siteUrl').value
  let date = Date.now()
  console.log(localStorage)
  if (localStorage.getItem("bookmarks") == null) {
    let id = 1
  } else {
    let id = JSON.parse(localStorage.getItem("bookmarks")).length + 1
  }
  let mark = {
    id,
    name,
    url,
    date
  }
  if (localStorage.getItem("bookmarks") != null) {
    let bookmarks = JSON.parse(localStorage.getItem("bookmarks"))
    bookmarks.push(mark)
    localStorage.setItem("bookmarks", JSON.stringify(bookmarks))
  } else {
    let bookmarks = []
    bookmarks.push(mark)
    localStorage.setItem("bookmarks", JSON.stringify(bookmarks))
  }
  fetchBookmarks()
  event.preventDefault()
}

function deleteBookmark(id) {}

function validURL(url) {}

function clearAll() {
  console.log('cleared?')
  let bookmarks = JSON.parse(localStorage.getItem("bookmarks"))
  if (!bookmarks.length) {
    bookmarks = []
    localStorage.setItem("bookmarks", JSON.stringify(bookmarks))
    fetchBookmarks()
    alert("Everything cleared! Add some new ones now :)")
  } else {
    fetchBookmarks()
    alert("You have none anyway.")
  }
}