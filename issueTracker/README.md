## Issues Tracker :clock10:

[Reference](https://www.youtube.com/watch?v=NYq9J-Eur9U)

Issue items will be stored on localstorage.

### Few observations :eyes:


> `<body onload="fetchIssues()">`
This is the entry point. 
This means the javascript function will be called whenever everything is loaded within the `<body>` tag. 

> `document.getElementById(id of form).addEventListener('submit', methodToRun)`
This grabs the form element and upon any event (submit in this case), it will run a specified method. There are no brackets after the name of the method because it is not to be run straight off but when the submit event occurs.

`event.preventDefault()` sits at the bottom of the function `saveIssue(event)` after an issue has been inputted, saved and resetted. 

#### Plan of Attack
1. `fetchIssues`: to grab anything from localStorage and display it on the page. Set up blank eventlisteners in the onclick attribute function
- this is easy to implement as it is calling localstorage, grab the parent html element, and interpolate the localStorage data as `innerHTML`
2. `saveIssue`: for grabbing user input when form is submitted, handling reset, passing it back up to localStorage
- involves a check for whether there is anything currently in the localStorage
- involves data validation
- need to stop the form from automatically reloading
3. go back and fill out the blank eventlisters `deleteIssue` and `setStatusClose`
- these are secondary functionalities 

### Local Storage
Some things are worth repeating for the sake of comprehension and banging it in place. 

Localstorage takes in key value pairs. Both of which are of `string`. Localstorage persists data in place when a page refresh. What ever for? irl, browser settings, user preferences (ie. darkmode and what not)
 
So to add data to localstorage, initialise an array and push objects into it with a reference id for each object. `localStorage.setItem("key", array)` can then turn the array into a JSON object for it to use. 

#### Javascript object :arrow_forward: localStorage JSON String
`JSON.stringify(object)` turns an object into a JSON string.

`object = { id: 1, desc: 'Hello it is i', status: true }` turns into `'{"id":1,"desc":"Hello it is i","status":true}'` which localStorage can then use.

#### localStorage JSON string :arrow_forward: Javascript
`JSON.parse(localStoragestring)` turns a JSON string back into a Javascript object. 


## Unresolved Mystery :bangbang:
String literals can't call the function passing an id argument in the onclick attribute. This (enclosed in backticks) does not work and throws up a syntax error

> `<a href="#" onclick = "setStatusClosed(\''+${id}+'\')" class="btn btn-warning">Close</a>`

Neither do this
> `<a href="#" onclick = "setStatusClosed(${id})" class="btn btn-warning">Close</a>`

Only as a string (enclosed with quotes) can you pass up the argument but need to enclose the `id` variable in literal quotes, as per follows
> `<a href="#" onclick="deleteIssue(\'' + id + '\')" class="btn btn-danger">Delete</a>`

Not sure why, need to ask. 