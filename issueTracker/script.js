// event listener to handle anytime the add button is clicked on the form
document.getElementById('issueInputForm').addEventListener('submit', saveIssue)

// when there is a submit event (add button gets pressed), we create a NEW issue
function saveIssue(event) {
  // Grab all user data from the form
  let issueDesc = document.getElementById('issueDescInput').value
  let issueSeverity = document.getElementById('issueSeverityInput').value
  let issueAssignedTo = document.getElementById('issueAssignedToInput').value
  let issueId = chance.guid()
  let issueStatus = 'Open' // Open is the default initial status, until closed by the user

  // Create an issue obj to hold user data, which can then be added an array
  let issue = {
    id: issueId,
    description: issueDesc,
    severity: issueSeverity,
    assignedTo: issueAssignedTo,
    status: issueStatus
  }

  // Add issue object onto localStorage
  // check if there is something in localStorage first
  if (localStorage.getItem('issues') == null) {
    let issues = []
    issues.push(issue)
    localStorage.setItem('issues', JSON.stringify(issues))
  } else {
    // get the issues JSON object back from localStorage first and turn it into a javascript array
    let issues = JSON.parse(localStorage.getItem("issues"))
    issues.push(issue) // add the new obj onto the array
    localStorage.setItem('issues', JSON.stringify(issues)) // turn the array back into a JSON object and send it back up to localStorage
  }

  // Reset form input elements
  document.getElementById('issueInputForm').reset() // everything in the form is re-initialised and any previous user input has been removed

  fetchIssues() // redisplay everything back onto the DOM

  event.preventDefault() // this needs to be at the end to prevent form from resubmitting automatically again
}

function setStatusClosed(id) {
  let issues = JSON.parse(localStorage.getItem("issues")) // returns a javascript array
  // change the status to Close for the object with the matching id
  for (let i = 0; i < issues.length; i++) {
    if (id === issues[i].id) {
      issues[i].status = 'Close'
    }
  }
  localStorage.setItem('issues', JSON.stringify(issues)) // send the updated array back up to localstorage
  fetchIssues() // redisplay everything back onto the DOM
}

function deleteIssue(id) {
  console.log(typeof id, "id is", id)
  // // get issues back from localStorage
  let issues = JSON.parse(localStorage.getItem("issues"))
  // filter out the one with matching id
  issues = issues.filter(issue => id != issue.id) // performant?
  alert("Issue deleted!") // user feedback
  // send it back up to localStorage
  localStorage.setItem("issues", JSON.stringify(issues))
  fetchIssues() // redisplay everything back onto the DOM
}

// Grab issues from the localStorage and display them on the page
// this runs whenever the <body> tag in the HTMl has been loaded and parsed onto the DOM
function fetchIssues() {
  // get all data against the key "issues" from the localStorage JSON object
  // convert the JSON back into javascript
  let issues = JSON.parse(localStorage.getItem("issues"))
  console.log('issues from localStorage:', issues)
  let issuesList = document.getElementById("issuesList") // grab the empty div under the form

  // default empty
  issuesList.innerHTML = ""

  // display every item in the issues array
  for (let i = 0; i < issues.length; i++) {
    // grab everything from localStorage
    let id = issues[i].id.toString()
    let description = issues[i].description
    let severity = issues[i].severity
    let assignedTo = issues[i].assignedTo
    let status = issues[i].status
    console.log(typeof id, id)

    // ***ERROR NOTE ***
    // Template literals could not pass up the ${id} which is WEIRD
    // It never even reaches the deleteIssues() method 
    // issuesList.innerHTML += `<div class="well">
    //   <h6>Issue ID: ${id}</h6>
    //   <p><span class="label label-info"> ${status}</span></p>  
    //   <h3>${description}</h3>
    //   <p><span class="glyphicon glyphicon-time"></span>${severity}</p>
    //   <p><span class="glyphicon glyphicon-user"></span>${assignedTo}</p>
    //   <a href="#" onclick = "setStatusClosed(\''+${id}+'\')" class="btn btn-warning">Close</a>
    //   <a href="#"
    //   onclick="deleteIssue(${id})"
    //   class="btn btn-danger">Delete</a>
    // </div>`

    // and display it on the DOM
    const div = document.createElement('div')
    div.classList.add('well')
    div.innerHTML =
      '<h6>Issue ID: ' + id + '</h6>' +
      '<p><span class="label label-info">' + status + '</span></p>' +
      '<h3>' + description + '</h3>' +
      '<p><span class="glyphicon glyphicon-time"></span> ' + severity + '</p>' +
      '<p><span class="glyphicon glyphicon-user"></span> ' + assignedTo + '</p>' +
      '<a href="#" onclick="setStatusClosed(\'' + id + '\')" class="btn btn-warning">Close</a> ' +
      '<a href="#" onclick="deleteIssue(\'' + id + '\')" class="btn btn-danger">Delete</a>'

    issuesList.appendChild(div)
  }
}