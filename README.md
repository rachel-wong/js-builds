# Mini Javascript Projects

Series of self-directed projects following tutorials and experimenting in own time to understand Javascript.

1. [Banking ATM](https://github.com/rachelwong/js-builds/tree/master/banking_app) - [Braindump](https://github.com/rachelwong/js-builds/blob/master/banking_app/README.md)
2. [Memory Card Game](https://github.com/rachelwong/js-builds/blob/master/memory_game/script.js) - [Braindump](https://github.com/rachelwong/js-builds/blob/master/memory_game/readme.md)
3. 01:Javascript30 - Javascript Drum kit - Completed with braindump
4. 02:Javascript30 - Clock - Completed with braindump
5. 03:Javascript30 - CSS Variables - Completed with braindump
6. 04:Javascript30 - Array Cardio - Completed with braindump
7. 05:Javascript30 - Flex panels - Completed with braindump
8. [Ghibli API Experiment](https://github.com/rachelwong/js-builds/tree/master/ghibliAPI) - Completed with braindump
9. 15: Local Storage To do list
10. 17: Sorting a list using regex to exclude certain words
11. 22: Follow along Links - in progress
12. 07: Array Cardio Day 2
