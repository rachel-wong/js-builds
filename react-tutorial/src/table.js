import React, { Component } from "react"

const TableHeader = () => {
	return (
		<thead>
			<tr>
				<th>Name</th>
				<th>Job</th>
			</tr>
		</thead>
	)
}

class Table extends Component {
	render() {
		// pass in the characterData object and the removeCharacter method
		const { characterData, removeCharacter } = this.props // can access all props through this.props
		return (
			<table>
				<TableHeader />
				<TableBody characterData={characterData} removeCharacter={removeCharacter} />
			</table>
		)
	}
}

// pass in props as an argument to TableBody from the class Table
const TableBody = props => {
	// pass the props into TableBody and generate rows for each object in the characters array in app.js
	const rows = props.characterData.map((row, index) => {
		return (
			<tr key={index}>
				<td>{row.name}</td>
				<td>{row.name}</td>
				<td>
					<button onClick={() => props.removeCharacter(index)}>Delete</button>
				</td>
			</tr>
		)
	})
	return <tbody>{rows}</tbody>
}

// class Table extends Component {
// 	render() {
// 		return (
// 			<table>
// 				<thead>
// 					<tr>
// 						<th>Name</th>
// 						<th>Job</th>
// 					</tr>
// 				</thead>
// 				<tbody>
// 					<tr>
// 						<td>Charlie</td>
// 						<td>Accountant</td>
// 					</tr>
// 					<tr>
// 						<td>John</td>
// 						<td>Developer</td>
// 					</tr>
// 					<tr>
// 						<td>Mary</td>
// 						<td>Customer Service</td>
// 					</tr>
// 					<tr>
// 						<td>Adam</td>
// 						<td>Writer</td>
// 					</tr>
// 				</tbody>
// 			</table>
// 		)
// 	}
// }

export default Table
