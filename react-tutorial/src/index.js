import React from "react"
import ReactDOM from "react-dom"
import App from "./App" // loading different parts of the page onto the main page as a component of React
import "./index.css"

ReactDOM.render(<App />, document.getElementById("root")) // this file is the entry point
