import React, { Component } from "react"
import Table from "./table"
//creates a component and loaded as a property of React in index.js
class App extends Component {
	// has all properties that is needed for the state
	state = {
		characters: [
			{
				name: "John",
				job: "Cafe Owner"
			},
			{
				name: "Matt",
				job: "Developer"
			},
			{
				name: "Chris",
				job: "Tester"
			},
			{
				name: "Mary",
				job: "Office Worker"
			}
		]
	}
	removeCharacter = index => {
		// retrieves the characters from the state object
		const { characters } = this.state

		// returns a new array where the characters don't include the one selected at index
		this.setState({
			characters: characters.filter((character, i) => {
				return i !== index
			})
		})
	}

	render() {
		// declare characters here to pull data from the state
		const { characters } = this.state

		//method to remove a character
		// characterData attribute is passing in the array objects created above called characters
		return (
			<div className='container'>
				<Table characterData={characters} removeCharacter={this.removeCharacter} />
			</div>
		)
	}
}
export default App // this exports it back to index.js
