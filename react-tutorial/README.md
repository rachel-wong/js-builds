# React single-page

Learning react by building a single web application with API call.

React is the View (UI) part of the MVC application.

Components are custom, reusable HTML elements.

Data is stored as state and handled as props.

To load any js libraries to start building a react app

```
npx create-react-app <name of the app or project>
```

To start the server and see your page

```
npm start
```

In react to interact with the DOM

> const nameOfVar = <htmlTag className="css class name">Whatever the content is </htmlTag>

Table.js is a customer class component in declaring a <table>. Convention is

```
import React, { Component } from "react"
class Name extends Component {
  render() {
    return (
      all the html content goes in here
    )
  }
}
```

You can break this up by creating separte simple components and then importing them inot the class. Simpel class componetn convention

```
const NameOfComponent = () => {
  return (
    all html content goes in here
  )
}
```

- for every simple component there is a `return`
- for every class component ther eis a `render` and `return`
- **return** can only return _one_ parent element
  Remember to export at the end

## Props

- simple components can't change props (read only)
- `this.props` access all properties
- intialise the data in app.js and append a data attribute to it. Pass it into HTML tag in the return statement in App.js
  > characterData={characters}
- in the component.js files, props need to be passed into the simple components in order to use it (the const)
- add a key index position for each row to manipulate data (i.e. add a delete button)

## State

- state allows you to manipulate data rather than just feeding data in using props
- `this.setState` is built in method to manipulate state objects
- need to declare the data (`this.state`) inside the render() of app.js in order to use it
