## What I learnt

Local storage (persisting state) is about when you refresh the page, the item you added to the DOM is still there.

### The todo list app

\_Added extra clear all and toggle all buttons that also updates the localStorage\*

EVent delegation (in this case) is about having a higher parent element to be listening for events. In this case <ul> will always exist, but the <li> might not.

Two phase to this

- listen for something higher
- Inside of it, check/filter out the thing that we actually want to do things with using `event.target.matches('whatever it is you do NOT want')`

```
  function toggleDone(e) {
    if (!e.target.matches('input')) return // this skips out all the events that are not inputs (labels)
    console.log(e.target)
  }
```

### Local Storage

To send UP to the browser for localStorage, need to convert any javascript object into JSON

> JSON.stringify(whatever it is)

To get it back down from the localStorage, you need to convert the JSON string back into an array of some kind

> JSON.parse(whatever the json thing is)

This allows user updated data to persist even when the page refreshes.
