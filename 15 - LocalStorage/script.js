  const addItems = document.querySelector('.add-items'); // the form, not the input box or button
  const itemsList = document.querySelector('.plates'); // the displayed list itself <ul>

  // the JSON.parse needs to be second to avoid throwing an error
  const items = [] || JSON.parse(localStorage.getItem("items")) // either the items array is populated with nothing, or whatever that is in localStorage where available
  const clearAll = document.querySelector('#clearBtn')
  const toggleAll = document.querySelector('#toggleBtn')

  function addItem(e) {
    e.preventDefault() //this stops the page from reloading. because by default, a form will send the data and reload.
    const text = this.querySelector('[name=item]').value // *this* is the form
    const item = {
      text,
      done: false,
    }

    items.push(item) // add item to the holding array items
    populateList(items, itemsList)
    // when you add to the list, you add to the localSTorage
    // if you just add items, it'll only show object object
    // localStorage only takes string
    // JSON.stringify converts any JS objects into string
    localStorage.setItem('items', JSON.stringify(items))
    this.reset() // resets the entire form
    // console.log(items)
  }

  function clearList(e) {
    localStorage.clear()
    items.splice(0, items.length) // clears the items array. itesm = [] doesn't work
    populateList(items, itemsList) // reupdates the list on the page
  }

  // Additional function 
  function toggleCheckboxes(e) {
    let completed = 0

    // first get total number of completed todos
    items.forEach(item => {
      if (item.done === true) {
        completed++
      }
    })

    // if all of the array is completed, then set everything to be false
    if (completed == items.length) {
      items.forEach(item => {
        item.done = !item.done
      })
    } else {
      // otherwise, set everything to be true
      items.forEach(item => {
        item.done = true
      })
    }
    console.log(items)
    // update localStorage, update list
    localStorage.setItem("items", JSON.stringify(items))
    populateList(items, itemsList)
  }

  // this enables the toggling and updating of the done status
  function toggleDone(e) {
    if (!e.target.matches('input')) return // this skips out all the events that are not inputs (labels)
    // need to specify which one of the li
    const el = e.target
    const index = el.dataset.position
    items[index].done = !items[index].done // when clicked, it updates the done status to whatever that it is NOT previously
    // console.log(items)
    localStorage.setItem("items", JSON.stringify(items)) // mirror this to the localSTorage
    populateList(items, itemsList) //update the list 
  }

  function populateList(plates = [], platesList) {
    // .map creates a new array from plates that prints out 
    // every element into an <li>
    // this is then attached to platesList which is an <ul>
    // .map takes an item from the array and position of the array
    platesList.innerHTML = plates.map((plate, i) => {
      return `
        <li>
          <input type="checkbox" data-position=${i} id="item${i}" ${plate.done? 'checked' : ''} />
          <label for="item${i}">${plate.text}</label>
        </li>
      `
    }).join("") // this removes the comma and space
  }
  addItems.addEventListener('submit', addItem)
  itemsList.addEventListener('click', toggleDone)
  clearAll.addEventListener('click', clearList)
  toggleAll.addEventListener('click', toggleCheckboxes)

  // don't put the JSON.parse(localStorage.getItems("items"))
  // set it up in the initialisation 
  populateList(items, itemsList)