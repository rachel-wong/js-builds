# URL Shortener from scratch

## Dependencies
- Express framework
- shortid
- valid-url
- config
- mongoose (for connecting to MongoDB)

Added two scripts to `package.json` to set index.js as the entry point for the server-side
>     "start": "node index", "dev": "nodemon index",

Created a new cluster on MongoDBAtlas



## Steps

#### Server set up and Database connection
- create basic server using express on index.js (include middleware .json to accept json data into the api)
- to store connections to the mongoDB, create a config folder with `default.json` for the config npm package to look into for connection
```
{
  "mongoURI": "mongodb+srv://<username></username>:<password>@cluster0-uzrds.mongodb.net/test?retryWrites=true&w=majority",
  "baseUrl": "http://localhost:5000" 
}
```
The baseURL is the domain if you deploy it
- create db.js in default folder to connect to the database itself
- bring in the connectDB from db.js into index.js

#### Create models
- create a schema for each resource
- create URL.js in models folder
