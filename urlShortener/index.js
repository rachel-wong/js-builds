const express = require('express')
const app = express()
const connectDB = require('./config/db')
const PORT = 5000

// CONNECT TO DB
connectDB()

// MIDDLEWARE
// because we need to send a post request in order to create a short URL, we need middleware to accept that data
app.use(express.json({
  extended: false
})) // this accepts json data into API

app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`)
})