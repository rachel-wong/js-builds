// Grab DOM elements here
const search = document.getElementById('search') // the search input form
const matchList = document.getElementById('match-list') // the results display div


// search states.json & filter
// fetch api returns a promise so async/await it
const searchStates = async searchText => {
  // wait for the fetch to finish
  const res = await fetch('states.json') // api url normally

  // fetch doesn't get the data right away
  // .json() reads the response and pass it back as a javscript array of objects
  const states = await res.json()

  // match with search input
  let matches = states.filter(state => {
    // get anything that starts with the search text and is case insensitive 
    const regex = new RegExp(`^${searchText}`, 'gi') // ^ means "starts with"
    return state.name.match(regex) || state.abbr.match(regex)
  })

  if (searchText.length === 0) {
    matches = [] // return nothing if there is nothing in the input field
    matchList.innerHTML = ""
  }
  console.log(matches)

  outputHtml(matches)
}

// helper to deal with displaying only
// Show results in HTML
const outputHtml = matches => {
  console.log("here", matches)
  if (matches.length > 0) {
    const html = matches
      .map(
        match => `<div class="card card-body mb-1">
    <h4>${match.name} (${match.abbr}) 
    <span class="text-primary">${match.capital}</span></h4>
    <small>Lat: ${match.lat} / Long: ${match.long}</small>
   </div>`
      )
      .join('')
    console.log(html)
    matchList.innerHTML = html
  } else {
    matchList.innerHTML = ""
  }
}


search.addEventListener('input', () => searchStates(search.value))