// get all triggers. things that will be hovered needing a background
const triggers = document.querySelectorAll('a')

// add highlight to the page
const highlight = document.createElement('span')
highlight.classList.add('highlight') // add highlight class to span
document.body.append(highlight) // add the span to the body

function highlightLink() {
  const linkCoords = this.getBoundingClientRect() // gives info on where on the page the thing lives 
  console.log(linkCoords)
  highlight.style.width = `${linkCoords.width}px`
}

triggers.forEach((a) => {
  a.addEventListener('mouseenter', highlightLink)
})