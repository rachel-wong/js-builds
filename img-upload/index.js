const express = require('express')
require('dotenv').config()
const multer = require('multer')
const cloudinary = require('cloudinary')
const PORT = 4000
const User = require('./models/User') // bring in model to add image to database
const fileUpload = require('express-fileupload')

// create instance of server app
const app = express()

// Sets up how the file will be stored
const storage = multer.diskStorage({
  filename: function (req, file, callback) {
    callback(null, Date.now() + file.originalname);
  }
})

// Checks for the file extension
const imageFilter = function (req, file, callback) {
  // accept image files only
  if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/i)) {
    return callback(new Error('Only image files are allowed!'), false)
  }2
  // otherwise return true to proceed with upload 
  return callback(null, true)
}

// add a file instance via multer
const upload = multer({
  storage: storage,
  fileFilter: imageFilter
})

// Cloudinary configuration
cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET
})

// Routes
app.get("/", (req, res, next) => {
  res.status(200).send("Hello World")
})

// Route to post image to cloudinary
app.post("/upload", function (req, res) {
  cloudinary.uploader.upload(req.file.path, function (result) {
    // add cloudinary_url for the image to the user object under image property
    req.body.User.image = result.secure_url
    console.log(result)
  })
})

app.listen(PORT, () => {
  console.log("Server is listening on port", PORT)
})