var mongoose = require("mongoose");

var UserSchema = new mongoose.Schema({
  name: String,
  image: String,
  imageId: String,
  description: String,
  comments: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Comment"
  }]
})

module.exports = mongoose.model("user", UserSchema);