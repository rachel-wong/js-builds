## CSS Variables - Wesbos Javascript30

#### Braindump

- `<input type="color">` changes the field to a colour picker
- css variables can be updated by javascript
- nodeList may have acccess to far fewer methods. so some cases might change the nodelist to an array.
- `data-` is an arbitrary attribute you can attach to html elements. 'data-' attributes will contains any attributes set to that element and `this.dataset` will place it into an object. so you can have `data-sizing` `data-length` `data-anything`
- you could just simply append "px" or whatever unit of measure to the value of the variable that you are changing. but using `data-` allows you to dynamically allocate the right unit of measure without hard coding
- how you would target a root css variable

```
document.documentElement.style.setProperty(`--base`)
```
