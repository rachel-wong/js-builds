// a list of cards and stored in a constant
const cards = document.querySelectorAll(".memory-card")

// when first card clicked/FLIPPED, WAIT for second card to click/FLIPPED
let hasFlippedCard = false // by default no card is clicked/flipped
let firstCard
let secondCard

function flipCard() {
	console.log("I was clicked")
	// this = is one .memory-card div
	// console.log(this)

	//toggle the flip class on each class on click
	// this.classList.toggle("flip")

	// when a card has click/flipped, add the class "flip" to the card
	this.classList.add("flip")

}

// loop through the list, for each card, attach an addevent listener and listen for click and execute a function called 'flipcard'
cards.forEach(card => card.addEventListener("click", flipCard))
