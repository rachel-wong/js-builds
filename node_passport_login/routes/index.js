  const express = require('express')
  const router = express.Router()

  //Home page route
  router.get('/', (req, res) => {
    res.render('welcome') // res.render the view called welcome which is in views/welcome.ejs
  })


  module.exports = router