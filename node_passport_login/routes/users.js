const express = require('express')
const router = express.Router()
const bcrypt = require('bcryptjs')

// User Model
const User = require('../models/User')

// Login Page routes
router.get('/login', (req, res) => {
  res.render('login') // when the route is /users/login, render whatever that is in views/login
})

// Register Page routes
router.get('/register', (req, res) => {
  res.render('register') // when the route is /users/register, render whatever that is in views/register
})

// Register handle - submit registration form
router.post('/register', (req, res) => {
  const {
    name,
    email,
    password,
    password2
  } = req.body // pulling specific attributes out from the req.body
  let errors = []

  // check if fields are filled 
  if (!name || !email || !password || !password2) {
    errors.push({
      msg: "Please fill in all fields."
    })
  }

  // Check if submitted passwords actually match
  if (password !== password2) {
    errors.push({
      msg: "Passwords do not match. "
    })
  }

  //Check password is at least 6 characters long
  if (password.length < 6) {
    errors.push({
      msg: "Passwords should be at least 6 characters long."
    })
  }

  // if there are errors, re-render the registration form by calling register.ejs
  // if there are errors, it will be rendered in the register.ejs through the messages partial
  if (errors.length > 0) {
    res.render('register', {
      errors,
      email,
      name,
      password,
      password2
    })
  } else {
    // Validation passes with no errors, find if an entry exists in the User model using email 
    User.findOne({
      email: email
    }).then(user => {
      //check for that user 
      if (user) {
        //user exists re render the register form and send along
        errors.push({
          msg: 'Email is already registered.'
        })
        res.render('register', {
          errors,
          email,
          name,
          password,
          password2
        })
      } else {
        // new model { values}
        const newUser = new User({
          name: name,
          email,
          password
        })
        console.log(newUser)
        res.send()
      }
    })
  }
})

module.exports = router