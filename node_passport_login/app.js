const express = require('express')
const app = express()
const PORT = process.env.PORT || 5000
const expressLayouts = require('express-ejs-layouts')
const mongoose = require('mongoose')

// DB Config
const db = require('./config/keys').MongoURI

// Connect to Mongo
mongoose.connect(db, {
  useNewUrlParser: true
}).then(() => console.log("mongo connected")).catch(err =>
  console.log(err))

// middleware EJS
app.use(expressLayouts) // this needs to be above the app.set
app.set('view engine', 'ejs')

// to get data from the form - bodyparser
app.use(express.urlencoded({
  extended: false
}))

// Routes
app.use('/', require('./routes/index')) // this sets what the route do to display the content in index.js
app.use('/users', require('./routes/users')) // this allows you to see pages in /users/login and /users/register

app.listen(PORT, console.log(`Server started on port ${PORT}`))