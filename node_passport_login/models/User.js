const Mongoose = require('mongoose')
const UserSchema = new Mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
})
// creates a model from the Schema
// const modlename = Mongoose.model('name of mdoel file', schemaName) 
const User = Mongoose.model('User', UserSchema)

module.exports = User