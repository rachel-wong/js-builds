import React, { Component } from "react"
import "./App.css"
import Form from "./components/Form"
import Recipes from "./components/Recipes"
const API_KEY = "eb53215ebbdf5219dfe073c0f22f78c5"

class App extends Component {
	// no need to bind methods in new version of react

	//new version of react dont' need the constructor function
	state = {
		// store the api results (array)
		recipes: []
	}

	// Event handler that makes the API call when the search button is clicked
	getRecipe = async e => {
		e.preventDefault()

		// this specifically targets whatever that is inputted in the input field and it is passed into the API call
		const recipeName = e.target.recipeName.value

		// cors-anywhere.herokuapp.com resolves the cors issue for dev purposes. Not idea for prod.
		const api_call = await fetch(
			`https://cors-anywhere.herokuapp.com/http://food2fork.com/api/search?key=${API_KEY}&q=${recipeName}&count=10`,
			{
				headers: {
					"Content-Type": "application/json",
					Accept: "application/json"
				}
			}
		)

		const data = await api_call.json() // convert the resuts of the api call into json which contains an array
		this.setState({ recipes: data.recipes }) // set the state to be the array contained inside the json that is returned from the api-call
		console.log(this.state.recipes)
	}
	
	// fetch everything from inside the localSTorage
	componentDidMount = () => {
		const json = localStorage.getItem("recipes")
		// need to convert localStorage back into javascript object
		const recipes = JSON.parse(json)
		// if the name of the property and the variable is the same, then you just need to declare/assign it once 
		this.setState({ recipes})
	}

	// keep the search results on the home page
	componentDidUpdate = () => {
		// make use of local storage to store the data
		// need to convert the state into json format first because local storage only takes json string
		const recipes = JSON.stringify(this.state.recipes)
		// assign it to localSTorage (name of the thing, the constabnt)
		localStorage.setItem("recipes", recipes)
	}

	
	render() {
		return (
			<div className='App'>
				<header className='App-header'>
					<h1 className='App-title'>Recipe Search</h1>
				</header>
				{/* the getRecipe prop handles all the api call and storing the result in the state */}
				<Form getRecipe={this.getRecipe} />
				{/* the recipes component takes the state from getRecipe and output the result separately */}
				<Recipes recipes={this.state.recipes} />
			</div>
		)
	}
}

export default App
