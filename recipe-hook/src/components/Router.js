//this renders out to the browser using Index.js
import React from "react"
import { BrowserRouter, Route, Switch } from "react-router-dom"

import App from "../App"
import Recipe from "./Recipe"

const Router = () => (
	<BrowserRouter>
		<Switch>
			{/* only render out the app component when the path is exactly "/"*/}
			<Route exact path='/' component={App} />
			{/* anything that comes after the : is considered a parameter/variable, in this case "id" */}
			<Route path='/recipe/:id' component={Recipe} />
		</Switch>
	</BrowserRouter>
)

export default Router
