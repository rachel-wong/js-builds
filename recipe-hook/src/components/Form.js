import React from "react"

const Form = props => (
	<form onSubmit={props.getRecipe} style={{ marginBottom: "2rem" }}>
		{/* set up the name attribute in <input> to read the value from the input */}
		<input className='form__input' type='text' name='recipeName' />
		<button className='form__button'>Search</button>
	</form>
)

export default Form
