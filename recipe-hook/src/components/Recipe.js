// individual recipes
import React from "react"
import {Link } from "react-router-dom"
const API_KEY = "eb53215ebbdf5219dfe073c0f22f78c5"

// goign to make use of state inside this component 
class Recipe extends React.Component {
  state = {
    activeRecipe: []
  }

  // this component fires when it is loaded/mounted on the browser
  componentDidMount = async () => {
    // props is passed in from the <Link> in recipes.js
    const title = this.props.location.state.recipe
    const req = await fetch(
			`https://cors-anywhere.herokuapp.com/http://food2fork.com/api/search?key=${API_KEY}&q=${title}`)
    const res = await req.json();
    this.setState({
      activeRecipe: res.recipes[0]
    })
    console.log(this.state.activeRecipe)

  }

  render() {
    const recipe = this.state.activeRecipe
    return (
      <div className="container">

        {/* check to see whether there is anything in the state before rendering anything at all on the page */}
        {this.state.activeRecipe.length !== 0 && 
          <div className="active-recipe">
          <img className="active-recipe__img" src={recipe.image_url} alt={recipe.title} />
          <h3 className="active-recipe__title">{ recipe.title}</h3>
          <h4 className="active-recipe__publisher">
          Publisher: 
          <span>{recipe.publisher}</span></h4>
          <p className="active-recipe__website">Website: <span>
            <a href={recipe.publisher_url}>{recipe.publisher_url}</a>
            </span>
          </p>
          <button className="active-recipe__button">
            <Link to="/">Go Home</Link>
          </button>
        </div>
        }
      </div>
    )
  }
}
export default Recipe
