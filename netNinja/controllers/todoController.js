const bodyParser = require('body-parser')
const urlencodedParser = bodyParser.urlencoded({
  extended: false
})
let data = [{
  item: 'get milk'
}, {
  item: 'walk dog'
}, {
  item: 'do something else'
}]

// module handling routes
module.exports = function (app) {

  app.get('/todo', (req, res) => {
    res.render('todo', {
      todos: data
    }) // this is referring to todo.ejs
  })


  app.post('/todo', urlencodedParser, function (req, res) {
    data.push(req.body)
    res.json(data)
  })
  
  app.delete('/todo', (req, res) => {

  })

  app.put('/todo', (req, res) => {

  })
}