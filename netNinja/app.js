const express = require('express')
const app = express()
const todoController = require('./controllers/todoController')

// set up templates in this project
app.set('view engine', 'ejs')
app.use(express.static('./public')) // middleware

todoController(app)

app.listen(3000, () => {
  console.log("server listening!")
})